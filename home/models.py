# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models

class AnimalHusbandary(models.Model):
    family_info = models.ForeignKey('FamilyInfo', models.DO_NOTHING)
    animal = models.ForeignKey('AnimalInfo', models.DO_NOTHING)
    animal_number = models.IntegerField(blank=True, null=True)
    selling_number = models.IntegerField(blank=True, null=True)
    death_number = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'animal_husbandary'




class AnimalInfo(models.Model):
    animal_id = models.AutoField(primary_key=True)
    animal_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'animal_info'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AwayAddress(models.Model):
    stay_id = models.AutoField(primary_key=True)
    place_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'away_address'


class AwayAddressTime(models.Model):
    member = models.ForeignKey('FamilyMember', models.DO_NOTHING)
    stay = models.ForeignKey(AwayAddress, models.DO_NOTHING)
    time_away_in_months = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'away_address_time'


class BankAccount(models.Model):
    bank_account_id = models.AutoField(primary_key=True)
    whose_account = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'bank_account'


class Caste(models.Model):
    caste_id = models.AutoField(primary_key=True)
    c_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'caste'


class Checkup(models.Model):
    household = models.ForeignKey('Household', models.DO_NOTHING)
    family_member_no = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    illness_type = models.ForeignKey('TypeIllness', models.DO_NOTHING, blank=True, null=True)
    checkup_place = models.ForeignKey('CheckupPlace', models.DO_NOTHING, blank=True, null=True)
    personcheckup = models.ForeignKey('PersonCheckup', models.DO_NOTHING, blank=True, null=True)
    treatment_exp = models.ForeignKey('TreatmentExpenses', models.DO_NOTHING, blank=True, null=True)
    reason_not_checkup = models.ForeignKey('ReasonNotCheckup', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'checkup'


class CheckupPlace(models.Model):
    checkup_place_id = models.AutoField(primary_key=True)
    check_place_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'checkup_place'


class ChildDetails(models.Model):
    household = models.ForeignKey('Household', models.DO_NOTHING)
    family_member_no = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    diarrehea = models.CharField(max_length=255, blank=True, null=True)
    bcg = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='bcg', blank=True, null=True, related_name='bcg_vaccination')
    opv1 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='opv1', blank=True, null=True , related_name='opv1_vaccination')
    opv2 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='opv2', blank=True, null=True, related_name='opv2_vaccination')
    opv3 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='opv3', blank=True, null=True, related_name='opv3_vaccination')
    pentualent1 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='pentualent1', blank=True, null=True, related_name='pent1_vaccination')
    pentualent2 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='pentualent2', blank=True, null=True, related_name='pent2_vaccination')
    pentualent3 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='pentualent3', blank=True, null=True, related_name='pent3_vaccination')
    pcv1 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='pcv1', blank=True, null=True, related_name='pcv1_vaccination')
    pcv2 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='pcv2', blank=True, null=True, related_name='pcv2_vaccination')
    pcv3 = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='pcv3', blank=True, null=True, related_name='pcv3_vaccination')
    measles_rubella = models.ForeignKey('Vaccination', models.DO_NOTHING, db_column='measles_rubella', blank=True, null=True)
    bgc_use = models.IntegerField(blank=True, null=True)
    times_polio = models.IntegerField(blank=True, null=True)
    times_pentualent = models.IntegerField(blank=True, null=True)
    times_pcv = models.IntegerField(blank=True, null=True)
    times_measles_rubella = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'child_details'


class CommunityOrg(models.Model):
    community_org_id = models.AutoField(primary_key=True)
    org_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'community_org'


class Complain(models.Model):
    complain_id = models.AutoField(primary_key=True)
    complain_place_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'complain'


class Crime(models.Model):
    crime_id = models.AutoField(primary_key=True)
    crime_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'crime'


class DeathInfo(models.Model):
    household = models.ForeignKey('Household', models.DO_NOTHING)
    reason = models.ForeignKey('DeathReason', models.DO_NOTHING)
    gender = models.CharField(max_length=255)
    age = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'death_info'


class DeathReason(models.Model):
    reason_id = models.AutoField(primary_key=True)
    reason_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'death_reason'


class Decision(models.Model):
    decision_id = models.AutoField(primary_key=True)
    decision_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'decision'


class DecisionMakers(models.Model):
    decision_makers_id = models.AutoField(primary_key=True)
    makers_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'decision_makers'


class DescriminationComplain(models.Model):
    household = models.ForeignKey('Household', models.DO_NOTHING)
    complain = models.ForeignKey(Complain, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'descrimination_complain'


class Destruction(models.Model):
    desctruction_id = models.AutoField(primary_key=True)
    destruction_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'destruction'


class DestructionByNaturalDisaster(models.Model):
    family_info = models.ForeignKey('FamilyInfo', models.DO_NOTHING)
    desctruction = models.ForeignKey(Destruction, models.DO_NOTHING)
    destruction_type = models.ForeignKey('DestructionType', models.DO_NOTHING)
    compensation_got = models.IntegerField(blank=True, null=True)
    number_of_human_loss = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destruction_by_natural_disaster'


class DestructionFromFlood(models.Model):
    family_info = models.ForeignKey('FamilyInfo', models.DO_NOTHING)
    desc_level = models.ForeignKey('DestructionLevel', models.DO_NOTHING)
    re_building = models.CharField(max_length=255, blank=True, null=True)
    earth_resistence = models.CharField(max_length=255, blank=True, null=True)
    amount_of_compensation = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destruction_from_flood'


class DestructionLevel(models.Model):
    desc_level_id = models.AutoField(primary_key=True)
    desc_level_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'destruction_level'


class DestructionType(models.Model):
    destruction_type_id = models.AutoField(primary_key=True)
    destruction_type = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destruction_type'


class DisabilityCard(models.Model):
    disability_card_id = models.AutoField(primary_key=True)
    disability_card_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'disability_card'


class DisabilityType(models.Model):
    disability_type_id = models.AutoField(primary_key=True)
    disability_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'disability_type'


class DisabilityTypeDisabilityCard(models.Model):
    member = models.ForeignKey('FamilyMember', models.DO_NOTHING)
    disability_type = models.ForeignKey(DisabilityType, models.DO_NOTHING)
    disability_card = models.ForeignKey(DisabilityCard, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'disability_type_disability_card'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Energy(models.Model):
    house_info = models.ForeignKey('HouseInfo', models.DO_NOTHING)
    main_energy = models.ForeignKey('EnergySource', models.DO_NOTHING, related_name='main_energy_source')
    second_energy = models.ForeignKey('EnergySource', models.DO_NOTHING, related_name='second_energy_source')
    time_to_collect_wood = models.IntegerField(blank=True, null=True)
    have_smokefree_kitchen = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'energy'


class EnergySource(models.Model):
    energy_id = models.AutoField(primary_key=True)
    energy_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'energy_source'


class Enumerators(models.Model):
    enum_id = models.AutoField(primary_key=True)
    enum_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'enumerators'


class Equipments(models.Model):
    equip_id = models.AutoField(primary_key=True)
    equip_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'equipments'


class EquipmentsFamilyInfo(models.Model):
    family_info = models.ForeignKey('FamilyInfo', models.DO_NOTHING)
    equip = models.ForeignKey(Equipments, models.DO_NOTHING)
    equip_quantity = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'equipments_family_info'


class FamilyDecision(models.Model):
    family_info = models.ForeignKey('FamilyInfo', models.DO_NOTHING)
    decision = models.ForeignKey(Decision, models.DO_NOTHING)
    decision_makers = models.ForeignKey(DecisionMakers, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'family_decision'


class FamilyImprovement(models.Model):
    improve_id = models.AutoField(primary_key=True)
    improvement_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'family_improvement'


class FamilyIncome(models.Model):
    family_info = models.ForeignKey('FamilyInfo', models.DO_NOTHING)
    income_source = models.ForeignKey('IncomeSource', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'family_income'


class FamilyInfo(models.Model):
    family_info_id = models.AutoField(primary_key=True)
    household = models.ForeignKey('Household', models.DO_NOTHING)
    voters_count = models.IntegerField()
    seperate_kitchen_member = models.IntegerField()
    ownership = models.ForeignKey('HouseOwnership', models.DO_NOTHING)
    government_health_insurance = models.CharField(max_length=255)
    life_insurance = models.CharField(max_length=255)
    bank_account = models.ForeignKey(BankAccount, models.DO_NOTHING)
    building_permit = models.CharField(max_length=255)
    karesabari = models.CharField(max_length=255)
    off_seasonal_farming = models.CharField(max_length=255)
    noland = models.ForeignKey('ReasonNoland', models.DO_NOTHING, blank=True, null=True)
    lease_land_area_ropani = models.FloatField(blank=True, null=True)
    modern_fertilizer = models.IntegerField(blank=True, null=True)
    use_organic_fertilizer = models.IntegerField(blank=True, null=True)
    num_of_month_fooding_by_agriculyural = models.IntegerField(blank=True, null=True)
    num_of_month_fooding_by_all_income_source = models.IntegerField(blank=True, null=True)
    famine_condition = models.ForeignKey('FamineCondition', models.DO_NOTHING, blank=True, null=True)
    death_due_cannot_afford_jhul_in_summer = models.IntegerField(blank=True, null=True)
    death_due_to_cooler = models.IntegerField(blank=True, null=True)
    improve_skill = models.ForeignKey('ImprovementSkill', models.DO_NOTHING, blank=True, null=True)
    family_improvement_firstpriority = models.ForeignKey(FamilyImprovement, models.DO_NOTHING, db_column='family_improvement_firstpriority', blank=True, null=True, related_name='family_first')
    family_improvement_secondpriority = models.ForeignKey(FamilyImprovement, models.DO_NOTHING, db_column='family_improvement_secondpriority', blank=True, null=True, related_name='family_second')
    family_improvement_thirdpriority = models.ForeignKey(FamilyImprovement, models.DO_NOTHING, db_column='family_improvement_thirdpriority', blank=True, null=True, related_name='family_third')
    society_improvement_firstpriority = models.ForeignKey('SocietyImprovement', models.DO_NOTHING, db_column='society_improvement_firstpriority', related_name='society_first')
    society_improvement_secondpriority = models.ForeignKey('SocietyImprovement', models.DO_NOTHING, db_column='society_improvement_secondpriority', related_name='society_second')
    society_improvement_thirdpriority = models.ForeignKey('SocietyImprovement', models.DO_NOTHING, db_column='society_improvement_thirdpriority', related_name='society_third')
    animal = models.ManyToManyField(AnimalInfo, blank=True)

    class Meta:
        managed = False
        db_table = 'family_info'


class FamilyInvolveInOrganzation(models.Model):
    household = models.ForeignKey('Household', models.DO_NOTHING)
    community_org = models.ForeignKey(CommunityOrg, models.DO_NOTHING)
    involved_member = models.ForeignKey('InvolvedMember', models.DO_NOTHING)
    board_of_directors = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'family_involve_in_organzation'


class FamilyMember(models.Model):
    member_id = models.AutoField(primary_key=True)
    household = models.ForeignKey('Household', models.DO_NOTHING)
    family_member_no = models.IntegerField(blank=True, null=True)
    member_name = models.CharField(max_length=255, blank=True, null=True)
    ho_relation = models.ForeignKey('Relation', models.DO_NOTHING, blank=True, null=True)
    gender = models.CharField(max_length=255, blank=True, null=True)
    completed_age_in_years = models.FloatField(blank=True, null=True)
    completed_age_in_months = models.IntegerField(blank=True, null=True)
    ssa = models.ForeignKey('SocialSecurityAllowance', models.DO_NOTHING, blank=True, null=True)
    birth_cetrificate = models.CharField(max_length=255, blank=True, null=True)
    marital = models.ForeignKey('MaritalStatus', models.DO_NOTHING, blank=True, null=True)
    citizenship = models.CharField(max_length=255, blank=True, null=True)
    can_write_letter = models.CharField(max_length=255, blank=True, null=True)
    reason_not_study = models.ForeignKey('NotStudyReason', models.DO_NOTHING, blank=True, null=True)
    occupation_main = models.ForeignKey('Occupation', models.DO_NOTHING, db_column='occupation_main', blank=True, null=True, related_name='occupation_main')
    occupation_sec = models.ForeignKey('Occupation', models.DO_NOTHING, db_column='occupation_sec', blank=True, null=True, related_name='occupation_sec')
    technical_training_done = models.CharField(max_length=255, blank=True, null=True)
    incurable_disease = models.ForeignKey('IncurableDisease', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'family_member'


class FamilySkill(models.Model):
    skill_id = models.AutoField(primary_key=True)
    skill_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'family_skill'


class FamilySkillUse(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    skill = models.ForeignKey(FamilySkill, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'family_skill_use'


class FamilyType(models.Model):
    family_type_id = models.AutoField(primary_key=True)
    type_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'family_type'


class FamineCondition(models.Model):
    famine_cond_id = models.AutoField(primary_key=True)
    famine_condition = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'famine_condition'

class FamineManagement(models.Model):
    famine_manage_id = models.AutoField(primary_key=True)
    management_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'famine_management'

class FamineManagementFamilyInfo(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    famine_manage = models.ForeignKey(FamineManagement, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'famine_management_family_info'


class FamineReason(models.Model):
    famine_reason_id = models.AutoField(primary_key=True)
    reason = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'famine_reason'


class FamineReasonFamilyInfo(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    famine_reason = models.ForeignKey(FamineReason, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'famine_reason_family_info'


class FarmInfo(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    farm = models.ForeignKey('Farming', models.DO_NOTHING)
    total_production = models.IntegerField(blank=True, null=True)
    unit = models.ForeignKey('ProductionUnit', models.DO_NOTHING, db_column='unit')
    self_used = models.IntegerField(blank=True, null=True)
    sold = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'farm_info'


class Farming(models.Model):
    farm_id = models.AutoField(primary_key=True)
    farm_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'farming'


class FemaleHealth(models.Model):
    household = models.ForeignKey('Household', models.DO_NOTHING, blank=True, null=True)
    place = models.ForeignKey('FertilityPlace', models.DO_NOTHING, blank=True, null=True)
    reason_nohospital = models.ForeignKey('NoUseHospital', models.DO_NOTHING, blank=True, null=True)
    age_of_household = models.IntegerField(blank=True, null=True)
    age_of_household_spouse = models.IntegerField(blank=True, null=True)
    times_of_checkup = models.IntegerField(blank=True, null=True)
    times_tt_vaccination = models.IntegerField(blank=True, null=True)
    times_of_use_ironcapsule = models.IntegerField(blank=True, null=True)
    juka_medicine = models.IntegerField(blank=True, null=True)
    vitamin_a = models.CharField(max_length=255, blank=True, null=True)
    age_when_first_child_born = models.IntegerField(blank=True, null=True)
    respondent_id = models.IntegerField(blank=True, null=True)
    respondent_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'female_health'


class FertilityPlace(models.Model):
    place_id = models.AutoField(primary_key=True)
    place_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fertility_place'


class GausalaWgs84(models.Model):
    gid = models.AutoField(primary_key=True)
    objectid_1 = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    objectid = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    dcode = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    district = models.CharField(max_length=50, blank=True, null=True)
    dan = models.CharField(max_length=50, blank=True, null=True)
    das = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    gapa_napa = models.CharField(max_length=50, blank=True, null=True)
    type_gn = models.CharField(max_length=50, blank=True, null=True)
    gn_code = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    new_ward_n = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    ddgnww = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    center = models.CharField(max_length=50, blank=True, null=True)
    state_code = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    ddgn = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    shape_leng = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ddgn_code = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    shape_le_1 = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    shape_area = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    ward = models.CharField(max_length=50, blank=True, null=True)
    geom = models.MultiPolygonField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gausala_wgs84'


class GovOrganization(models.Model):
    gov_id = models.AutoField(primary_key=True)
    gov_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'gov_organization'


class HouseInDanger(models.Model):
    house_danger_id = models.AutoField(primary_key=True)
    house_danger = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'house_in_danger'


class HouseInfo(models.Model):
    house_info_id = models.AutoField(primary_key=True)
    household = models.ForeignKey('Household', models.DO_NOTHING)
    toilet_type = models.ForeignKey('ToiletType', models.DO_NOTHING)
    light_source = models.ForeignKey('LightSource', models.DO_NOTHING)
    roof_cons = models.ForeignKey('RoofConstructionMaterial', models.DO_NOTHING, related_name='house_info')
    wall_cons = models.ForeignKey('WallConstructionMaterial', models.DO_NOTHING)
    house_danger = models.ForeignKey(HouseInDanger, models.DO_NOTHING)
    total_no_of_floor = models.IntegerField(blank=True, null=True)
    water_source = models.ForeignKey('WaterSource', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'house_info'


class HouseOwnership(models.Model):
    ownership_id = models.AutoField(primary_key=True)
    ownship_condition = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'house_ownership'


class Household(models.Model):
    household_id = models.AutoField(primary_key=True)
    owner_fname = models.CharField(max_length=255)
    owner_lname = models.CharField(max_length=255)
    enum = models.ForeignKey(Enumerators, models.DO_NOTHING)
    caste = models.ForeignKey(Caste, models.DO_NOTHING)
    religion = models.ForeignKey('Religion', models.DO_NOTHING)
    language = models.ForeignKey('Language', models.DO_NOTHING)
    vdc = models.ForeignKey('PastVdc', models.DO_NOTHING, blank=True, null=True)
    family_type = models.ForeignKey(FamilyType, models.DO_NOTHING)
    langauge_spoken = models.CharField(max_length=255)
    past_ward_no = models.IntegerField(blank=True, null=True)
    present_ward_no = models.IntegerField(blank=True, null=True)
    tole_name = models.CharField(max_length=255)
    ph_number = models.CharField(max_length=255, blank=True, null=True)
    no_of_member = models.IntegerField()
    suggestion = models.CharField(max_length=255)
    gender = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'household'

    def __str__(self):
        return self.owner_fname


class HouseholdCrime(models.Model):
    household = models.ForeignKey(Household, models.DO_NOTHING)
    crime = models.ForeignKey(Crime, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'household_crime'


class ImprovementSkill(models.Model):
    improve_skill_id = models.AutoField(primary_key=True)
    improve_skill_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'improvement_skill'


class IncomeSource(models.Model):
    income_source_id = models.AutoField(primary_key=True)
    source_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'income_source'


class IncurableDisease(models.Model):
    incurable_disease_id = models.AutoField(primary_key=True)
    disease_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'incurable_disease'


class InvolvedMember(models.Model):
    involved_member_id = models.AutoField(primary_key=True)
    member_data = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'involved_member'


class LandInRopani(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    land_info = models.ForeignKey('LandInfo', models.DO_NOTHING)
    tweleve_month_irrigation = models.FloatField(blank=True, null=True)
    irrigation_in_rainy_season_only = models.FloatField(blank=True, null=True)
    no_irrigtaion = models.FloatField(blank=True, null=True)
    total_area = models.FloatField(blank=True, null=True)
    land_with_female_ownership = models.FloatField(blank=True, null=True)
    barren_land = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'land_in_ropani'


class LandInfo(models.Model):
    land_info_id = models.AutoField(primary_key=True)
    land_type = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'land_info'


class Language(models.Model):
    language_id = models.AutoField(primary_key=True)
    language_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'language'


class LightSource(models.Model):
    light_source_id = models.AutoField(primary_key=True)
    li_source_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'light_source'


class LocalServices(models.Model):
    household = models.ForeignKey(Household, models.DO_NOTHING)
    gov = models.ForeignKey(GovOrganization, models.DO_NOTHING, blank=True, null=True)
    transport = models.ForeignKey('Transportation', models.DO_NOTHING)
    time_period = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'local_services'


class Location(models.Model):
    household = models.ForeignKey(Household, models.DO_NOTHING)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    altitude = models.FloatField(blank=True, null=True)
    geom = models.GeometryField(srid=0, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'location'

class MaritalStatus(models.Model):
    marital_id = models.AutoField(primary_key=True)
    marital_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'marital_status'

class Migration(models.Model):
    household = models.ForeignKey(Household, models.DO_NOTHING)
    migration_reason = models.ForeignKey('MigrationReason', models.DO_NOTHING, related_name='reason_one')
    mig_from = models.ForeignKey('MigrationFrom', models.DO_NOTHING)
    migrating_here_reason = models.ForeignKey('MigrationReason', models.DO_NOTHING, db_column='migrating_here_reason', related_name='reason_two')

    class Meta:
        managed = False
        db_table = 'migration'


class MigrationFrom(models.Model):
    mig_from_id = models.AutoField(primary_key=True)
    mig_from_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'migration_from'


class MigrationReason(models.Model):
    mig_id = models.AutoField(primary_key=True)
    mig_reason = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'migration_reason'


class NoUseHospital(models.Model):
    reason_nohospital_id = models.AutoField(primary_key=True)
    reason_no_hospital = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'no_use_hospital'


class NotStudyReason(models.Model):
    reason_not_study_id = models.AutoField(primary_key=True)
    reason_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'not_study_reason'


class Occupation(models.Model):
    occu_id = models.AutoField(primary_key=True)
    occu_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'occupation'


class Office(models.Model):
    office_id = models.AutoField(primary_key=True)
    office_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'office'


class PassedEducationLevel(models.Model):
    passed_edu_id = models.AutoField(primary_key=True)
    level_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'passed_education_level'


class PastVdc(models.Model):
    vdc_id = models.AutoField(primary_key=True)
    vdc_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'past_vdc'


class PersonCheckup(models.Model):
    personcheckup_id = models.AutoField(primary_key=True)
    personcheckup_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'person_checkup'


class PointcloudFormats(models.Model):
    pcid = models.IntegerField(primary_key=True)
    srid = models.IntegerField(blank=True, null=True)
    schema = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pointcloud_formats'


class Problems(models.Model):
    problem_reason_id = models.AutoField(primary_key=True)
    problem_reason_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'problems'


class ProblemsFacedInOffice(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    problem_reason = models.ForeignKey(Problems, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'problems_faced_in_office'


class ProductionUnit(models.Model):
    unit_id = models.AutoField(primary_key=True)
    unit_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'production_unit'


class ReasonNoland(models.Model):
    noland_id = models.AutoField(primary_key=True)
    reason = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'reason_noland'


class ReasonNotCheckup(models.Model):
    reason_not_id = models.AutoField(primary_key=True)
    reason_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'reason_not_checkup'


class Register(models.Model):
    register_id = models.AutoField(primary_key=True)
    register_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'register'


class RegisterData(models.Model):
    household = models.ForeignKey(Household, models.DO_NOTHING)
    reg_place = models.ForeignKey('RegistrationPlaces', models.DO_NOTHING, blank=True, null=True)
    register = models.ForeignKey(Register, models.DO_NOTHING)
    record_in_three_years = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'register_data'


class RegistrationPlaces(models.Model):
    reg_place_id = models.AutoField(primary_key=True)
    reg_place_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'registration_places'


class RegularitySchool(models.Model):
    regularity_id = models.AutoField(primary_key=True)
    regularity_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'regularity_school'


class Relation(models.Model):
    ho_relation_id = models.AutoField(primary_key=True)
    relation_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'relation'


class Religion(models.Model):
    religion_id = models.AutoField(primary_key=True)
    religion_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'religion'


class RemitExpense(models.Model):
    expense_id = models.AutoField(primary_key=True)
    expense_title = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'remit_expense'


class Remittance(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    remit_amount = models.IntegerField()
    remit_expense_prim = models.ForeignKey(RemitExpense, models.DO_NOTHING, db_column='remit_expense_prim', related_name='remit_prim')
    remit_expense_sec = models.ForeignKey(RemitExpense, models.DO_NOTHING, db_column='remit_expense_sec', related_name='remit_sec')

    class Meta:
        managed = False
        db_table = 'remittance'


class RoofConstructionMaterial(models.Model):
    roof_cons_id = models.AutoField(primary_key=True)
    cons_material = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'roof_construction_material'

    def __str__(self):
        return self.cons_material


class SchoolType(models.Model):
    school_type_id = models.AutoField(primary_key=True)
    school_type_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'school_type'


class ScialPlace(models.Model):
    social_places_id = models.AutoField(primary_key=True)
    place_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'scial_place'


class ServicesOffice(models.Model):
    family_info = models.ForeignKey(FamilyInfo, models.DO_NOTHING)
    office = models.ForeignKey(Office, models.DO_NOTHING, blank=True, null=True)
    receipt = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'services_office'


class SocialDiscrimination(models.Model):
    household = models.ForeignKey(Household, models.DO_NOTHING)
    social_places = models.ForeignKey(ScialPlace, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'social_discrimination'


class SocialSecurityAllowance(models.Model):
    ssa_id = models.AutoField(primary_key=True)
    ssa_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'social_security_allowance'


class SocietyImprovement(models.Model):
    social_improve_id = models.AutoField(primary_key=True)
    social_improve_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'society_improvement'


class Study(models.Model):
    member = models.ForeignKey(FamilyMember, models.DO_NOTHING)
    passed_edu = models.ForeignKey(PassedEducationLevel, models.DO_NOTHING, blank=True, null=True)
    studying_edu = models.ForeignKey('StudyingEducationLevel', models.DO_NOTHING, blank=True, null=True)
    school_type = models.ForeignKey(SchoolType, models.DO_NOTHING, blank=True, null=True)
    regularity = models.ForeignKey(RegularitySchool, models.DO_NOTHING, blank=True, null=True)
    scholarship = models.CharField(max_length=255, blank=True, null=True)
    work_leaving_study = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'study'


class StudyingEducationLevel(models.Model):
    studying_edu_id = models.AutoField(primary_key=True)
    studying_level = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'studying_education_level'


class Tax(models.Model):
    tax_id = models.AutoField(primary_key=True)
    tax_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tax'


class TaxHousehold(models.Model):
    household = models.ForeignKey(Household, models.DO_NOTHING)
    tax = models.ForeignKey(Tax, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tax_household'


class ToiletType(models.Model):
    toilet_type_id = models.AutoField(primary_key=True)
    toilet_type_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'toilet_type'


class Transportation(models.Model):
    transport_id = models.AutoField(primary_key=True)
    transportation_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'transportation'


class TreatmentExpenses(models.Model):
    treatment_exp_id = models.AutoField(primary_key=True)
    treatment_exp_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'treatment_expenses'


class TypeIllness(models.Model):
    illness_type_id = models.AutoField(primary_key=True)
    illness_type_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'type_illness'


class Vaccination(models.Model):
    vaccination_id = models.AutoField(primary_key=True)
    vaccination_yes_no = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'vaccination'


class WallConstructionMaterial(models.Model):
    wall_cons_id = models.AutoField(primary_key=True)
    material_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wall_construction_material'


class WasteManagement(models.Model):
    waste_id = models.AutoField(primary_key=True)
    waste_manage_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'waste_management'

class WasteMaterial(models.Model):
    house_info = models.ForeignKey(HouseInfo, models.DO_NOTHING)
    waste = models.ForeignKey(WasteManagement, models.DO_NOTHING)
    deg_non_deg = models.CharField(max_length=255, blank=True, null=True)
    yes_no = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'waste_material'


class WaterSource(models.Model):
    water_source_id = models.AutoField(primary_key=True)
    water_source_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'water_source'

class MunicipalityPolygonWgs84(models.Model):
    gid = models.AutoField(primary_key=True)
    gn_code = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    geom = models.MultiPolygonField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'municipality_polygon_wgs84'