from django.shortcuts import render
from django.http import HttpResponse
from django.http.response import json, JsonResponse
from django.db.models import Count, Q, F, When, Case, Value
from django.views.generic import TemplateView,ListView, View, FormView
from django.contrib.auth.decorators import login_required
from django.urls import reverse, reverse_lazy
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login, logout
from .forms import UserForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.views import LoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import UserForm
from .models import AnimalInfo, AnimalHusbandary, FarmInfo, Farming, RoofConstructionMaterial, HouseInfo, Caste, \
    Household, DeathReason, DeathInfo, DestructionType, DestructionByNaturalDisaster,\
    FamilyMember, IncurableDisease, Equipments, EquipmentsFamilyInfo,\
    FamilyImprovement, FamilyInfo, FamineReason, FamineReasonFamilyInfo, IncomeSource, FamilyIncome, \
    MaritalStatus, Destruction, HouseOwnership, Religion, SchoolType, Study, SocietyImprovement, Tax, TaxHousehold, \
    ToiletType, WallConstructionMaterial, WaterSource, EnergySource, FamilyType, Language, LightSource, \
    PassedEducationLevel, StudyingEducationLevel



dict_area={
1:18651073.79 ,
2:14078440.11 ,
3:5273810.55 ,
4:6476207.32 ,
5:1646070.99 ,
6:3004073.44 ,
7:8974473.66 ,
8:30570223.85 ,
9:9645541.25 ,
10:12779930.95 ,
11:9730722.43 ,
12:23904255.28 ,
99:674676629.63 }


def calculate_percentage(total, fetch):
    data = []
    for i in fetch:
        percentage = (i['y'] / total) * 100
        i['y'] = round(percentage, 2)
        if i['y'] > 0.29:
            data.append(i)
    return data


class Gaushala(LoginRequiredMixin, ListView):
    template_name = 'home.html'

    def get_queryset(self):
        pass


def register(request):
    registered = False
    if request.method == "POST":
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            print("sucessfully registered user")
            registered = True

        else:
            print("Invalid form request")

    else:
        user_form = UserForm()

    return render(request, 'registration.html', {
            'user_form': user_form,
            'registered': registered
        })

class Login(LoginView):
    template_name = 'login.html'

    def get_success_url(self):
        return reverse_lazy('home')

@login_required
def user_logout(request):
    logout(request) #session is destroyed
    status=1
    return HttpResponseRedirect(reverse('login'))


class Allinone(Gaushala, View):

    def get(self, request):
        data = {}
        ward = int(request.GET.get("ward"))

        if ward == 99:
            # population
            data['population'] = []
            total_population = FamilyMember.objects.all().count()

            male = FamilyMember.objects.filter(gender='1').count()
            female = FamilyMember.objects.filter(gender='2').count()

            child_0_to_4 = FamilyMember.objects.filter(completed_age_in_years__range=(0, 14)).count()
            working_15_to_64 = FamilyMember.objects.filter(completed_age_in_years__range=(15, 64)).count()
            aging = FamilyMember.objects.filter(completed_age_in_years__gte=65).count()

            num_of_household = Household.objects.all().count()

            sum_familysize_of_each_household = Household.objects.values('familymember__member_id').count()
            average_family_size_of_each_household = round(sum_familysize_of_each_household / num_of_household, 0)

            sex_ratio = male / female
            population_density = total_population / dict_area[ward]
            male_house_owner = Household.objects.filter(gender='1').count()
            female_house_owner = Household.objects.filter(gender='2').count()

            data.update({'population': [total_population, male, female, child_0_to_4, working_15_to_64,
                                        aging, num_of_household, average_family_size_of_each_household, sex_ratio,
                                        population_density, male_house_owner, female_house_owner]})



            #calculate in terms of house_info
            total_house_info = HouseInfo.objects.count()

            # fetch_toilet_type
            fetch_toilet_type = ToiletType.objects.values(name=F('toilet_type_name')).annotate(
                y=(Count('houseinfo'))).filter(y__gt=0)

            data['fetch_toilet_type'] = calculate_percentage(total_house_info, fetch_toilet_type)



            # fetch_wall
            fetch_wall = WallConstructionMaterial.objects.values(name=F('material_name')).annotate(
                y=(Count('houseinfo'))).filter(y__gt=0).order_by('-y')

            data['fetch_wall'] = calculate_percentage(total_house_info, fetch_wall)



            # fetch_roof
            fetch_roof = RoofConstructionMaterial.objects.values(name=F('cons_material')).annotate(
                y=(Count('house_info'))).filter(y__gt=0).order_by('-y')

            data['fetch_roof'] = calculate_percentage(total_house_info, fetch_roof)



            # fetch_water_source
            fetch_water_source = WaterSource.objects.values(name=F('water_source_name')).annotate(
                y=(Count('houseinfo'))).filter(y__gt=0).order_by('y')

            data['fetch_water_source'] = calculate_percentage(total_house_info, fetch_water_source)



            # fetch_cooking_fuel
            fetch_cooking_fuel = EnergySource.objects.values(name=F('energy_name')).annotate(
                y=(Count('main_energy_source'))).filter(y__gt=0).order_by('y')

            data['fetch_cooking_fuel'] = calculate_percentage(total_house_info, fetch_cooking_fuel)



            # fetch_light_source
            fetch_light_source = LightSource.objects.values(name=F('li_source_name')).annotate(
                y=(Count('houseinfo'))).filter(y__gt=0).order_by('y')

            data['fetch_light_source'] = calculate_percentage(total_house_info, fetch_light_source)



            #calculate in terms of household
            total_household = Household.objects.count()

            # fetch_caste
            fetch_caste = Caste.objects.values(name=F('c_name')).annotate(
                y=(Count('household'))).filter(y__gt=0).order_by('y')

            data['fetch_caste'] = calculate_percentage(total_household, fetch_caste)
            print("---------------------------------------")
            print(data['fetch_caste'])



            # fetch_family_type
            fetch_family_type = FamilyType.objects.values(name=F('type_name')).annotate(
                y=(Count('household'))).filter(y__gt=0)

            data['fetch_family_type'] = calculate_percentage(total_household, fetch_family_type)



            # fetch_langugage_percentage
            fetch_language_percentage = Language.objects.values(name=F('language_name')).annotate(
                y=(Count('household'))).filter(y__gt=0).order_by('y')

            data['fetch_language_percentage'] = calculate_percentage(total_household, fetch_language_percentage)



            # fetch_religion
            fetch_religion = Religion.objects.values('religion_name').annotate(
                y=(Count('household'))).filter(y__gt=0).order_by('-y')

            data['fetch_religion'] = calculate_percentage(total_household, fetch_religion)




            #calculate interms of familyinfo
            family_info = FamilyInfo.objects.count()

            # fetch_family_improvements
            fetch_family_improvements = FamilyImprovement.objects.values(name=F('improvement_name')).annotate(
                y=(Count('family_first'))).filter(y__gt=0).order_by('-y')

            data['fetch_family_improvements'] = calculate_percentage(family_info, fetch_family_improvements)



            # fetch_society_improvements
            fetch_society_improvements = SocietyImprovement.objects.values(name=F('social_improve_name')).annotate(y=(
                Count('society_first'))).filter(y__gt=0).order_by('y')

            data['fetch_society_improvements'] = calculate_percentage(family_info, fetch_society_improvements)



            # fetch_ownership_condition
            fetch_ownership_condition = HouseOwnership.objects.values(name=F('ownship_condition')).annotate(
                y=(Count('familyinfo'))).filter(y__gt=0).order_by('y')

            data['fetch_ownership_condition'] = calculate_percentage(family_info, fetch_ownership_condition)



            #---------------------------------------------------------------------------------#
            # fetch_animal
            fetch_animal = AnimalInfo.objects.values(name=F('animal_name')).annotate(
                y=(Count('animalhusbandary'))).filter(y__gt=0).order_by('y')

            total_animal_husbandary = AnimalHusbandary.objects.count()

            data['fetch_animal'] = calculate_percentage(total_animal_husbandary, fetch_animal)



            # fetch agriculture_production
            fetch_agriculture_production = Farming.objects.values(name=F('farm_name')).annotate(
                y=(Count('farminfo'))).filter(y__gt=0).order_by('y')

            total_farm = FarmInfo.objects.count()

            data['fetch_agriculture_production'] = calculate_percentage(total_farm, fetch_agriculture_production)



            # fetch_death_reason
            fetch_death_reason = DeathReason.objects.values(name=F('reason_name')).annotate(
                y=(Count('deathinfo'))).filter(y__gt=0).order_by('-y')

            total_death = DeathInfo.objects.count()

            data['fetch_death_reason'] = calculate_percentage(total_death, fetch_death_reason)



            # fetch_destruction_type
            fetch_destruction_type = DestructionType.objects.values(name=F('destruction_type')).annotate(
                y=(Count('destructionbynaturaldisaster'))).filter(
                y__gt=0).order_by('y')

            total_destruction_by_natural_disaster = DestructionByNaturalDisaster.objects.filter(
                destruction_type_id__isnull=False).count()

            data['fetch_destruction_type'] = calculate_percentage(total_destruction_by_natural_disaster,
                                                                  fetch_destruction_type)
            # data.update({'total_destruction': total_destruction_by_natural_disaster})



            # fetch_disease_percentage
            data['disease_notdisease'] = []
            fetch_disease_percentage = IncurableDisease.objects.values(name=F('disease_name')).annotate(
                y=(Count('familymember'))).filter(y__gt=0).order_by('y')

            total_family_member = FamilyMember.objects.filter(incurable_disease_id__isnull=False).count()
            total_not_diseased = total_population - total_family_member
            data['fetch_disease_percentage'] = calculate_percentage(total_family_member, fetch_disease_percentage)
            data.update({'disease_notdisease': [total_family_member, total_not_diseased]})



            # fetch_equipments
            fetch_equipments = Equipments.objects.values(name=F('equip_name')).annotate(
                y=Count('equipmentsfamilyinfo')).filter(y__gt=0).order_by('y')

            total_equipments_family_info = EquipmentsFamilyInfo.objects.count()

            data['fetch_equipments'] = calculate_percentage(total_equipments_family_info, fetch_equipments)



            # fetch_famine_reason
            fetch_famine_reason = FamineReason.objects.values(name=F('reason')).annotate(
                y=Count('faminereasonfamilyinfo')).filter(
                y__gt=0).order_by('-y')

            total_famine_reason_family_info = FamineReasonFamilyInfo.objects.count()

            data['fetch_famine_reason'] = calculate_percentage(total_famine_reason_family_info, fetch_famine_reason)



            # fetch_income_source
            fetch_income_source = IncomeSource.objects.values(name=F('source_name')).annotate(
                y=Count('familyincome')).filter(y__gt=0).order_by('y')

            total_family_income = FamilyIncome.objects.count()

            data['fetch_income_source'] = calculate_percentage(total_family_income, fetch_income_source)



            # fetch_marital_status
            fetch_marital_status = MaritalStatus.objects.values(name=F('marital_name')).annotate(
                y=(Count('familymember'))).filter(y__gt=0).order_by('-y')

            total_marital_status = FamilyMember.objects.filter(marital_id__gt=0).count()

            data['fetch_marital_status'] = calculate_percentage(total_marital_status, fetch_marital_status)



            # fetch_natural_disaster_destruction
            fetch_natural_disaster_destruction = Destruction.objects.values(name=F('destruction_name')).annotate(
                y=(Count('destructionbynaturaldisaster'))).filter(y__gt=0).order_by('-y')

            total_destruction = DestructionByNaturalDisaster.objects.count()

            data['fetch_natural_disaster_destruction'] = calculate_percentage(total_destruction,
                                                                              fetch_natural_disaster_destruction)



            # fetch_school_type
            fetch_school_type = SchoolType.objects.values(name=F('school_type_name')).annotate(
                y=(Count('study'))).filter(y__gt=0)

            total_school_type = Study.objects.filter(school_type_id__gt=0).count()

            data['fetch_school_type'] = calculate_percentage(total_school_type, fetch_school_type)



            # fetch_tax
            fetch_tax = Tax.objects.values(name=F('tax_name')).annotate(
                y=(Count('taxhousehold'))).filter(y__gt=0).order_by('-y')

            total_tax = TaxHousehold.objects.filter(tax_id__gt=0).count()

            data['fetch_tax'] = calculate_percentage(total_tax, fetch_tax)



            # passed_education_level
            data['passed_and_notpassed'] = []
            passed_education_level = PassedEducationLevel.objects.values(name=F('level_name')).annotate(
                y=(Count('study'))).filter(y__gt=0).order_by(
                'y')

            total_passed = Study.objects.filter(passed_edu_id__isnull=False).count()
            total_not_passed = total_population - total_passed

            data['passed_education_level'] = calculate_percentage(total_passed, passed_education_level)
            data.update({'passed_and_notpassed': [total_passed, total_not_passed]})



            # studying_education_level
            data['studying_notstudying'] = []
            studying_education_level = StudyingEducationLevel.objects.values(name=F('studying_level')).annotate(
                y=(Count('study'))).filter(y__gt=0).order_by('y')

            total_study = Study.objects.filter(studying_edu_id__isnull=False).count()

            total_not_study = total_population - total_study - total_passed

            data['studying_education_level'] = calculate_percentage(total_study, studying_education_level)
            data.update({'studying_notstudying': [total_study, total_not_study]})



            # fetch_education_by_sex
            fetch_education_by_sex = PassedEducationLevel.objects.values('level_name').annotate(
                male=(Count(Case(When(study__member__gender='1', then=1)))),
                female=(Count(Case(When(study__member__gender='2', then=1)))),
                other=(Count(Case(When(study__member__gender='99', then=1))))
            ).filter(Q(male__gt=0) | Q(female__gt=0) | Q(other__gt=0)).order_by('female')

            data['fetch_education_by_sex'] = {}
            level = [];
            male = [];
            female = [];
            other = []

            for i in fetch_education_by_sex:
                level.append(i['level_name'])
                male.append(i['male'])
                female.append(i['female'])
                other.append(i['other'])

            data.update({'fetch_education_by_sex': {'level': level, 'male': male, 'female': female, 'other': other}})



            # fetch_death_by_sex
            fetch_death_by_sex = DeathReason.objects.values('reason_name').annotate(
                male=(Count(Case(When(deathinfo__gender='1', then=1)))),
                female=(Count(Case(When(deathinfo__gender='2', then=1)))),
                other=(Count(Case(When(deathinfo__gender='99', then=1))))
            ).filter(Q(male__gt=0) | Q(female__gt=0) | Q(other__gt=0)).order_by('male')

            data['fetch_death_by_sex'] = {}
            reason = [];
            male = [];
            female = [];
            other = []

            for i in fetch_death_by_sex:
                reason.append(i['reason_name'])
                male.append(i['male'])
                female.append(i['female'])
                other.append(i['other'])

            data.update({'fetch_death_by_sex': {'reason': reason, 'male': male, 'female': female, 'other': other}})



            # population_pyramid_by_age_and_sex
            data['population_pyramid'] = []
            male_range = []
            female_range = []
            i = 0
            while i <= 90:
                m = FamilyMember.objects.filter(gender=1).filter(completed_age_in_years__range=(i, 4 + i)).count()
                f = FamilyMember.objects.filter(gender=2).filter(completed_age_in_years__range=(i, 4 + i)).count()
                i += 5
                male_range.append(-m)
                female_range.append(f)
            m_90 = FamilyMember.objects.filter(gender=1).filter(completed_age_in_years__gt=89).count()
            f_90 = FamilyMember.objects.filter(gender=2).filter(completed_age_in_years__gt=89).count()
            male_range.append(-m_90)
            female_range.append(-f_90)
            data.update({'population_pyramid': {'male_range': male_range, 'female_range': female_range}})


        else:
            # population
            data['population'] = []
            total_population = FamilyMember.objects.filter(household__present_ward_no=ward).count()

            male = FamilyMember.objects.filter(gender='1', household__present_ward_no=ward).count()
            female = FamilyMember.objects.filter(gender='2', household__present_ward_no=ward).count()

            child_0_to_4 = FamilyMember.objects.filter(completed_age_in_years__range=(0, 14),
                                                       household__present_ward_no=ward).count()
            working_15_to_64 = FamilyMember.objects.filter(completed_age_in_years__range=(15, 64),
                                                           household__present_ward_no=ward).count()
            aging = FamilyMember.objects.filter(completed_age_in_years__gte=65,
                                                household__present_ward_no=ward).count()

            num_of_household = Household.objects.filter(present_ward_no=ward).count()

            sum_familysize_of_each_household = Household.objects.values('familymember__member_id').filter(
                present_ward_no=ward).count()
            average_family_size_of_each_household = round(sum_familysize_of_each_household / num_of_household, 0)

            sex_ratio = male / female
            population_density = total_population / dict_area[ward]
            male_house_owner = Household.objects.filter(gender='1', present_ward_no=ward).count()
            female_house_owner = Household.objects.filter(gender='2', present_ward_no=ward).count()

            data.update({'population': [total_population, male, female, child_0_to_4, working_15_to_64, aging,
                                        num_of_household, average_family_size_of_each_household, sex_ratio,
                                        population_density, male_house_owner, female_house_owner]})



            #fetch_animal
            fetch_animal = AnimalInfo.objects.values(name=F('animal_name')).annotate(
                y=(Count('animalhusbandary',
                             filter=Q(animalhusbandary__family_info__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_animal_husbandary = AnimalHusbandary.objects.filter(family_info__household__present_ward_no=ward).count()

            data['fetch_animal'] = calculate_percentage(total_animal_husbandary, fetch_animal)



            # fetch agriculture_production
            fetch_agriculture_production = Farming.objects.values(name=F('farm_name')).annotate(
                y=(Count('farminfo', filter=Q(farminfo__family_info__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_farm = FarmInfo.objects.filter(family_info__household__present_ward_no=ward).count()

            data['fetch_agriculture_production'] = calculate_percentage(total_farm, fetch_agriculture_production)



            # fetch_roof
            fetch_roof = RoofConstructionMaterial.objects.values(name=F('cons_material')).annotate(
                y=(Count('house_info', filter=Q(house_info__household__present_ward_no=ward)))).filter(y__gt=0).order_by('-y')

            total_house = HouseInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_roof'] = calculate_percentage(total_house, fetch_roof)



            # fetch_caste
            fetch_caste = Caste.objects.values(name=F('c_name')).annotate(
                y=(Count('household', filter=Q(household__present_ward_no=ward)))).filter(y__gt=0).filter(y__gt=0).order_by('-y')

            total_household = Household.objects.filter(present_ward_no=ward).count()

            data['fetch_caste'] = calculate_percentage(total_household, fetch_caste)



            # fetch_death_reason
            fetch_death_reason = DeathReason.objects.values(name=F('reason_name')).annotate(
                y=(Count('deathinfo', filter=Q(deathinfo__household__present_ward_no=ward)))).filter(y__gt=0).order_by('-y')

            total_death = DeathInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_death_reason'] = calculate_percentage(total_death, fetch_death_reason)



            # fetch_destruction_type
            fetch_destruction_type = DestructionType.objects.values(name=F('destruction_type')).annotate(
                y=(Count('destructionbynaturaldisaster',
                             filter=Q(destructionbynaturaldisaster__family_info__household__present_ward_no=ward)))).filter(y__gt=0).order_by(
                'y')

            total_destruction_by_natural_disaster = DestructionByNaturalDisaster.objects.filter(
                destruction_type_id__isnull=False).filter(family_info__household__present_ward_no=ward).count()

            data['fetch_destruction_type'] = calculate_percentage(total_destruction_by_natural_disaster, fetch_destruction_type)
            # data.update({'total_destruction': total_destruction_by_natural_disaster})



            # fetch_disease_percentage
            data['disease_notdisease'] = []
            fetch_disease_percentage = IncurableDisease.objects.values(name=F('disease_name')).annotate(
                y=(Count('familymember', filter=Q(familymember__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_family_member = FamilyMember.objects.filter(incurable_disease_id__isnull=False).filter(household__present_ward_no=ward).count()

            total_not_diseased = total_population - total_family_member
            data['fetch_disease_percentage'] = calculate_percentage(total_family_member, fetch_disease_percentage)
            data.update({'disease_notdisease': [total_family_member, total_not_diseased]})


            # fetch_equipments
            fetch_equipments = Equipments.objects.values(name=F('equip_name')).annotate(
                y=Count('equipmentsfamilyinfo',
                            filter=Q(equipmentsfamilyinfo__family_info__household__present_ward_no=ward))).filter(y__gt=0).order_by('y')

            total_equipments_family_info = EquipmentsFamilyInfo.objects.filter(
                family_info__household__present_ward_no=ward).count()

            data['fetch_equipments'] = calculate_percentage(total_equipments_family_info, fetch_equipments)



            # fetch_family_improvements
            fetch_family_improvements = FamilyImprovement.objects.values(name=F('improvement_name')).annotate(
                y=(Count('family_first', filter=Q(family_first__household__present_ward_no=ward)))).filter(y__gt=0).order_by('-y')

            total_family_first = FamilyInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_family_improvements'] = calculate_percentage(total_family_first, fetch_family_improvements)



            # fetch_famine_reason
            fetch_famine_reason = FamineReason.objects.values(name=F('reason')).annotate(
                y=Count('faminereasonfamilyinfo',
                            filter=Q(faminereasonfamilyinfo__family_info__household__present_ward_no=ward))).filter(y__gt=0).order_by('-y')

            total_famine_reason_family_info = FamineReasonFamilyInfo.objects.filter(
                family_info__household__present_ward_no=ward).count()

            data['fetch_famine_reason'] = calculate_percentage(total_famine_reason_family_info, fetch_famine_reason)



            # fetch_income_source
            fetch_income_source = IncomeSource.objects.values(name=F('source_name')).annotate(
                y=Count('familyincome', filter=Q(familyincome__family_info__household__present_ward_no=ward))).filter(y__gt=0).order_by(
                'y')

            total_family_income = FamilyIncome.objects.filter(family_info__household__present_ward_no=ward).count()

            data['fetch_income_source'] = calculate_percentage(total_family_income, fetch_income_source)



            # fetch_marital_status
            fetch_marital_status = MaritalStatus.objects.values(name=F('marital_name')).annotate(
                y=(Count('familymember', filter=Q(familymember__household__present_ward_no=ward)))).filter(y__gt=0).order_by('-y')

            total_marital_status = FamilyMember.objects.filter(marital_id__gt=0).filter(
                household__present_ward_no=ward).count()

            data['fetch_marital_status'] = calculate_percentage(total_marital_status, fetch_marital_status)



            # fetch_natural_disaster_destruction
            fetch_natural_disaster_destruction = Destruction.objects.values(name=F('destruction_name')).annotate(
                y=(Count('destructionbynaturaldisaster',
                             filter=Q(destructionbynaturaldisaster__family_info__household__present_ward_no=ward)))).filter(y__gt=0).order_by(
                '-y')

            total_destruction = DestructionByNaturalDisaster.objects.filter(
                family_info__household__present_ward_no=ward).count()

            data['fetch_natural_disaster_destruction'] = calculate_percentage(total_destruction, fetch_natural_disaster_destruction)



            # fetch_ownership_condition
            fetch_ownership_condition = HouseOwnership.objects.values(name=F('ownship_condition')).annotate(
                y=(Count('familyinfo', filter=Q(familyinfo__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_familyinfo = FamilyInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_ownership_condition'] = calculate_percentage(total_familyinfo, fetch_ownership_condition)



            # fetch_religion
            fetch_religion = Religion.objects.values('religion_name').annotate(
                y=(Count('household', filter=Q(household__present_ward_no=ward)))).filter(y__gt=0).order_by('-y')

            total_household = Household.objects.filter(present_ward_no=ward).count()

            data['fetch_religion'] = calculate_percentage(total_household, fetch_religion)



            # fetch_school_type
            fetch_school_type = SchoolType.objects.values(name=F('school_type_name')).annotate(
                y=(Count('study', filter=Q(study__member__household__present_ward_no=ward)))).filter(y__gt=0)

            total_school_type = Study.objects.filter(school_type_id__gt=0).filter(
                member__household__present_ward_no=ward).count()

            data['fetch_school_type'] = calculate_percentage(total_school_type, fetch_school_type)



            # fetch_society_improvements
            fetch_society_improvements = SocietyImprovement.objects.values(name=F('social_improve_name')).annotate(y=(
                Count('society_first', filter=Q(society_first__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_society_first = FamilyInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_society_improvements'] = calculate_percentage(total_society_first, fetch_society_improvements)



            # fetch_tax
            fetch_tax = Tax.objects.values(name=F('tax_name')).annotate(
                y=(Count('taxhousehold', filter=Q(taxhousehold__household__present_ward_no=ward)))).filter(y__gt=0).order_by('-y')

            total_tax = TaxHousehold.objects.filter(tax_id__gt=0).filter(household__present_ward_no=ward).count()

            data['fetch_tax'] = calculate_percentage(total_tax, fetch_tax)



            # fetch_toilet_type
            fetch_toilet_type = ToiletType.objects.values(name=F('toilet_type_name')).annotate(
                y=(Count('houseinfo', filter=Q(houseinfo__household__present_ward_no=ward)))).filter(y__gt=0)

            total_household = HouseInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_toilet_type'] = calculate_percentage(total_household, fetch_toilet_type)



            # fetch_wall
            fetch_wall = WallConstructionMaterial.objects.values(name=F('material_name')).annotate(
                y=(Count('houseinfo', filter=Q(houseinfo__household__present_ward_no=ward)))).filter(y__gt=0).order_by('-y')

            total_household = HouseInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_wall'] = calculate_percentage(total_household, fetch_wall)



            # fetch_water_source
            fetch_water_source = WaterSource.objects.values(name=F('water_source_name')).annotate(
                y=(Count('houseinfo', filter=Q(houseinfo__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_household = HouseInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_water_source'] = calculate_percentage(total_household, fetch_water_source)



            # fetch_cooking_fuel
            fetch_cooking_fuel = EnergySource.objects.values(name=F('energy_name')).annotate(
                y=(Count('main_energy_source',
                             filter=Q(main_energy_source__house_info__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_household = HouseInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_cooking_fuel'] = calculate_percentage(total_household, fetch_cooking_fuel)



            # fetch_family_type
            fetch_family_type = FamilyType.objects.values(name=F('type_name')).annotate(
                y=(Count('household', filter=Q(household__present_ward_no=ward)))).filter(y__gt=0)

            total_household = Household.objects.filter(present_ward_no=ward).count()

            data['fetch_family_type'] = calculate_percentage(total_household, fetch_family_type)



            # fetch_langugage_percentage
            fetch_language_percentage = Language.objects.values(name=F('language_name')).annotate(
                y=(Count('household', filter=Q(household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_household = Household.objects.filter(present_ward_no=ward).count()

            data['fetch_language_percentage'] = calculate_percentage(total_household, fetch_language_percentage)



            # fetch_light_source
            fetch_light_source = LightSource.objects.values(name=F('li_source_name')).annotate(
                y=(Count('houseinfo', filter=Q(houseinfo__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_house_info = HouseInfo.objects.filter(household__present_ward_no=ward).count()

            data['fetch_light_source'] = calculate_percentage(total_house_info, fetch_light_source)



            # passed_education_level
            data['passed_and_notpassed'] = []
            passed_education_level = PassedEducationLevel.objects.values(name=F('level_name')).annotate(
                y=(Count('study', filter=Q(study__member__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_passed = Study.objects.filter(passed_edu_id__isnull=False).filter(
                member__household__present_ward_no=ward).count()
            total_not_passed = total_population - total_passed
            data['passed_education_level'] = calculate_percentage(total_passed, passed_education_level)
            data.update({'passed_and_notpassed': [total_passed, total_not_passed]})



            # studying_education_level
            data['studying_notstudying'] = []
            studying_education_level = StudyingEducationLevel.objects.values(name=F('studying_level')).annotate(
                y=(Count('study', filter=Q(study__member__household__present_ward_no=ward)))).filter(y__gt=0).order_by('y')

            total_study = Study.objects.filter(studying_edu_id__isnull=False).filter(
                member__household__present_ward_no=ward).count()
            total_not_study = total_population - total_study - total_passed

            data['studying_education_level'] = calculate_percentage(total_study, studying_education_level)
            data.update({'studying_notstudying': [total_study, total_not_study]})



            # fetch_education_by_sex
            fetch_education_by_sex = PassedEducationLevel.objects.values('level_name').annotate(
               male=(Count(Case(When(study__member__gender='1', then=1)),
                            filter=Q(study__member__household__present_ward_no=ward))),
                female=(Count(Case(When(study__member__gender='2', then=1)),
                              filter=Q(study__member__household__present_ward_no=ward))),
                other=(Count(Case(When(study__member__gender='99', then=1)),
                             filter=Q(study__member__household__present_ward_no=ward)))
            ).filter(Q(male__gt=0) | Q(female__gt=0) | Q(other__gt=0)).order_by('female')

            data['fetch_education_by_sex']={}
            level = []; male = []; female = []; other = []

            for i in fetch_education_by_sex:
                level.append(i['level_name'])
                male.append(i['male'])
                female.append(i['female'])
                other.append(i['other'])

            data.update({'fetch_education_by_sex': {'level': level, 'male': male, 'female': female, 'other': other}})



            # fetch_death_by_sex
            fetch_death_by_sex = DeathReason.objects.values('reason_name').annotate(
                male=(Count(Case(When(deathinfo__gender='1', then=1)),
                            filter=Q(deathinfo__household__present_ward_no=ward))),
                female=(Count(Case(When(deathinfo__gender='2', then=1)),
                            filter=Q(deathinfo__household__present_ward_no=ward))),
                other=(Count(Case(When(deathinfo__gender='99', then=1)),
                             filter=Q(deathinfo__household__present_ward_no=ward)))
                ).filter(Q(male__gt=0) | Q(female__gt=0) | Q(other__gt=0)).order_by('male')

            data['fetch_death_by_sex']={}
            reason = []; male = []; female = []; other = []

            for i in fetch_death_by_sex:
                reason.append(i['reason_name'])
                male.append(i['male'])
                female.append(i['female'])
                other.append(i['other'])

            data.update({'fetch_death_by_sex': {'reason': reason, 'male': male, 'female': female, 'other': other}})



            #population_pyramid_by_age_and_sex
            data['population_pyramid']=[]
            male_range = []
            female_range = []
            i = 0
            while i <= 85:
                m = FamilyMember.objects.filter(gender=1).filter(completed_age_in_years__range=(i, 4+i)).filter(
                    household__present_ward_no=ward).count()
                f = FamilyMember.objects.filter(gender=2).filter(completed_age_in_years__range=(i, 4+i)).filter(
                    household__present_ward_no=ward).count()
                i += 5
                male_range.append(-m)
                female_range.append(f)
            m_90 = FamilyMember.objects.filter(gender=1).filter(completed_age_in_years__gt=89).filter(
                household__present_ward_no=ward).count()
            f_90 = FamilyMember.objects.filter(gender=2).filter(completed_age_in_years__gt=89).filter(
                household__present_ward_no=ward).count()
            male_range.append(-m_90)
            female_range.append(f_90)
            data.update({'population_pyramid': {'male_range': male_range, 'female_range': female_range}})
            print(data)
        return JsonResponse(data)



