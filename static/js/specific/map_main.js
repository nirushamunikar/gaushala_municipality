$(document).ready(function () {
    //for popups
    var extent = {
        1: [85.79860957500006, 26.919830947000037, 85.85824443700005, 27.002156607000035]
        , 2: [85.75339718400005, 26.86764409700004, 85.80371262900005, 26.91675068500004]
        , 3: [85.80140375200006, 26.89542352600006, 85.83384629700005, 26.922416758000058]
        , 4: [85.78508528600008, 26.907239323000056, 85.82304770800009, 26.937318751000056]
        , 5: [85.77467187600007, 26.91476048100003, 85.79027022100007, 26.93086628300003]
        , 6: [85.73924599400004, 26.899541162000048, 85.77069150600005, 26.91762035100005]
        , 7: [85.70507079400005, 26.89825172600007, 85.74043271600006, 26.933596718000068]
        , 8: [85.77610293500004, 26.928039127000034, 85.85180380400004, 27.006191310000034]
        , 9: [85.73625332200004, 26.914473901000065, 85.77823978000004, 26.939938349000066]
        , 10: [85.75846449800008, 26.938598934000026, 85.79536294300009, 27.008343807000028]
        , 11: [85.73955221800009, 26.935347789000048, 85.77265271800009, 26.986915989000046]
        , 12: [85.71569722800007, 26.92698156000006, 85.78198102800006, 27.012801775000057]
        , 99: [85.7050707940001, 26.867644097, 85.8582444370001, 27.0128017750001]
    };
    // ########## deep dai Design
    var project_selected;
    var baseurl = window.location.hostname;
    // console.log(baseurl);
    //Sidebar toggle
    $('button.navbar-toggle').on('click', function () {
        $('.sidebar ').toggleClass('is-active');
    });
    //show-hide export
    $(".export-icon").on("click", function (e) {
        $(".export").addClass("expanded");
        $(".export-icon").hide();
        $(".results-icon").hide();
        $("html, body").animate({
            scrollTop: $(document).height()
        }, 1000);
    });
    $(".close-export").on("click", function (e) {
        $(".export").removeClass("expanded");
        $(".export-icon").show();
        $(".results-icon").show();
    });
    //show-hide results
    $(".results-icon").on("click", function (e) {
        $(".results").addClass("expanded");
        $(".results-icon").hide();
        $(".export-icon").hide();
        $("html, body").animate({
            scrollTop: $(document).height()
        }, 1000);
    });
    $(".results .close-button").on("click", function (e) {
        $(".results").removeClass("expanded");
        $(".results-icon").show();
        $(".export-icon").show();
    });
    //Sidebar toggle
    $('button.navbar-toggle').on('click', function () {
        $('.sidebar').toggleClass('is-active');
    });
    //Tooltip
    $('[data-toggle="tooltip"]').tooltip({
        'placement': 'top'
    });
    //Scrollbar
    $('.scrollbar-macosx').scrollbar();


    /** Sticky Header **/
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.header-navigation').addClass("sticky-header").css('top', '-150px');
            $("#brand-logo").attr("src", "images/logo.png");
        } else {
            $('.header-navigation').removeClass("sticky-header").css('top', '0');
            $("#brand-logo").attr("src", "images/logo-white.png");
        }

        if ($(this).scrollTop() > 500) {
            $('.sticky-header').addClass("slow").css('top', '0').fadeTo(500, 1);
        } else {
            $('.sticky-header').removeClass("slow");
            $('.sticky-header').css('top', '-150px');
        }
    });

    // Declare Carousel jquery object
    var owl = $('.banner-wrap');

    // Carousel initialization
    owl.owlCarousel({
        loop: false,
        margin: 0,
        navSpeed: 500,
        nav: false,
        pagination: true,
        autoplay: true,
        autoplayHoverPause: true,
        rewind: true,
        items: 1
    });


    // add animate.css class(es) to the elements to be animated
    function setAnimation(_elem, _InOut) {
        // Store all animationend event name in a string.
        // cf animate.css documentation
        var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

        _elem.each(function () {
            var $elem = $(this);
            var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

            $elem.addClass($animationType).one(animationEndEvent, function () {
                $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
            });
        });
    }

    // Fired before current slide change
    owl.on('change.owl.carousel', function (event) {
        var $currentItem = $('.owl-item', owl).eq(event.item.index);
        var $elemsToanim = $currentItem.find("[data-animation-out]");
        setAnimation($elemsToanim, 'out');
    });

    // Fired after current slide has been changed
    var round = 0;
    owl.on('changed.owl.carousel', function (event) {

        var $currentItem = $('.owl-item', owl).eq(event.item.index);
        var $elemsToanim = $currentItem.find("[data-animation-in]");

        setAnimation($elemsToanim, 'in');
    })

    owl.on('translated.owl.carousel', function (event) {
        console.log(event.item.index, event.page.count);

        if (event.item.index == (event.page.count - 1)) {
            if (round < 1) {
                round++
                console.log(round);
            } else {
                owl.trigger('stop.owl.autoplay');
                var owlData = owl.data('owl.carousel');
                owlData.settings.autoplay = true; //don't know if both are necessary
                owlData.options.autoplay = true;
                owl.trigger('refresh.owl.carousel');
            }
        }
    });


    /*Equal MaxHeight  */
    var elements = $('.js-equal-height');
    elements.removeAttr('style');
    var maxHeight = -1;
    elements.each(function () {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });
    elements.each(function () {
        $(this).height(maxHeight);
    });
    $(window).resize(function () {
        elements.removeAttr('style');
        setTimeout(function () {
            var maxHeight = -1;
            elements.each(function () {
                maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
            });
            elements.each(function () {
                $(this).height(maxHeight);
            })
        }, 500)
    });

    /*Equal MaxHeight  */
    var elements = $('.js-equal-height2');
    elements.removeAttr('style');
    var maxHeight = -1;
    elements.each(function () {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });
    elements.each(function () {
        $(this).height(maxHeight);
    });

    $(window).resize(function () {
        elements.removeAttr('style');
        setTimeout(function () {
            var maxHeight = -1;
            elements.each(function () {
                maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
            });
            elements.each(function () {
                $(this).height(maxHeight);
            })
        }, 500)
    });
    //Bootstrap Carousel
    $(".carousel").swipe({

        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {

            if (direction == 'left') $(this).carousel('next');
            if (direction == 'right') $(this).carousel('prev');

        },
        allowPageScroll: "vertical"

    });


    //sidebar active
    $('button.navbar-toggle').on('click', function () {
        $('.sidebar ').toggleClass('is-active');
    });

    //Fancybox
    $('.fancybox').fancybox({
        helpers: {
            title: {
                type: 'inside'
            },
        },
    });


    $('.time-carousel').owlCarousel({
        center: true,
        loop: false,
        items: 1,
        nav: true
    });

    $('.chart-carousel').owlCarousel({
        loop: true,
        items: 3,
        margin: 10,
        nav: true,
        navSpeed: 1000,
        autoplayHoverPause: true,
        autoplay: false,
    });

    $(document).on('click', '.owl-item>div', function () {
        $owl.trigger('to.owl.carousel', $(this).data('position'));
    });

    //on click item selection
    $('.section').find('.list-style li').on('click', function (elem) {
        $("li").removeClass("current-item");
        $(this).addClass('current-item');
        if (that.itemClicked) {
            that.itemClicked('hello i am here');
        }
        // console.log(elem);
    });

    // ############### Ending of Design Section ###############################


    init();


    //for base maps
    $(document).on("click", ".map-wrap", function () {
        // $(".map-wrap").click(function () {
        var bmap = $(this).text().trim();
        console.log(bmap);
        if (bmap == "None") {
            osmLayer.setVisible(false);
            bingLayer.setVisible(false);
        } else if (bmap == "OSM") {
            osmLayer.setVisible(true);
            bingLayer.setVisible(false);
        } else {
            bingLayer.setVisible(true);
            osmLayer.setVisible(false);
        }
    });
    // for layer on off

    $(".layer").click(function () {

        var id = $(this).attr("id");
        var l = getLayer(id);
        console.log("name:" + l.toString());
        var legend = getLayFeature(id);
        var title = getLayTitle(id);
        console.log(title);

        if ($(this).prop("checked") == true) {
            l.setVisible(true);

        } else {
            l.setVisible(false);
        }
    });


    $(".check-layer").each(function () {

        var id = $(this).attr("id");
        // var l = getLayer(id);
        var legend = getLayFeature(id);
        // var title = getLayTitle(id);
        // var markup = "<img name=" + id + " src=" + legend + "&legend_options=fontName:Times%20New%20Roman;fontAntiAliasing:true;fontColor:0x000033;fontSize:9;dpi:150>";
        var markup = "<img name=" + id + " src=" + legend + ">";
        $(markup).prependTo($(this).parents().children("div.label-img"));
    });


    $(".close-button").click(function () {
        $(this).parent().removeClass('active in');
        $('.icon-bar > .nav-tabs > li').removeClass('active');
    });


    var button_geo = '<div class="geolocation ol-unselectable ol-control added-button" id="geolocation-on">';
    button_geo += '<button type="button" title="Geolocation on off"><img class="svg-icon-ol-interaction" src="/static/img/locationIcon.svg"/></button>';

    button_geo += '</div>'

    $('.ol-zoom-extent button').empty();
    $('.ol-zoom-extent button').append('<img class="svg-icon-ol-extent" src="/static/gaushala/img/extent.svg"/>');
    var mySVGsToInject3 = document.querySelectorAll('img.svg-icon-ol-extent');

    // Do the injection
    SVGInjector(mySVGsToInject3);

    // var offset_value = $('.ol-overlaycontainer-stopevent .ol-zoom-extent').offset();
    // console.log(offset_value);
    // $('#geolocation-on').css({"left":String(offset_value.left), "top":String(offset_value.top+10)})
    var button_p_ext = '<div class="project-extent ol-unselectable ol-control added-button" id="project-extent-zoom">';
    button_p_ext += '<button type="button" title="Go to Project Extent"><img class="svg-icon-ol-interaction" src="/static/img/zoom-to-extent.svg"/></button>';

    button_p_ext += '</div>'


    // $('#project-extent').css({"left":String(offset_value.left)+'px', "top":String(offset_value.top+20)+'px'})
    // $(document).on("click", "#geolocation-on", function() {
    // $('.results-icon').css('opacity','0');
    $('.results-icon').hide();
    $('#switch-map a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var currentTab = $(e.target).text(); // get current tab

        // console.log(currentTab);
        var target = map.getTarget();
        if (currentTab == 'Exit Full Screen') {
            // $('#geolocation-on').remove();
            // $('#project-extent-zoom').remove();
            $(".results").removeClass("expanded");
            $('.results-icon').hide();
            // highlightOverlay.getSource().clear();
            // drawSource.clear();
            // clusterLayer.getSource().clear();

            if ($('#ppp').length) // use this if you are using id to check
            {
                console.log('kkkkkkkkk');
                project_selected = $('#ppp').attr("projectID");
                console.log(project_selected);
                // $('#projectname').empty();
            }
            map.setTarget('map');
        } else if (currentTab == 'Enter Full Screen') {
            map.setTarget('map-enlarged');
            // $('.ol-overlaycontainer-stopevent').append(button_geo);
            // $('.ol-overlaycontainer-stopevent').append(button_p_ext);
            // var mySVGsToInject2 = document.querySelectorAll('img.svg-icon-ol-interaction');

            // // Do the injection
            // SVGInjector(mySVGsToInject2);

            $('.results-icon').show();
        }
    });

    $('.gutter-lt a').on('click', function () {
        console.log($(this).text());
        var txt = $(this).text();
        console.log($(this).hasClass('collapsed'));
        if ($(this).hasClass('collapsed')) {
            console.log('ok');
            $('.sidebar .select-box .timeline span').empty();
            $('.sidebar .select-box .timeline span').append(txt);
        } else {
            $('.sidebar .select-box .timeline span').empty();
            $('.sidebar .select-box .timeline span').append('Select Group Layer');
        }
    })

    $('.select-picker').select2({
        width: '100%'
    });
    $('#year-select-infoGraphics').on('change', function () {
        console.log(selected_adminid);
        var scode = $('.selState').val();
        var dcode = $('.selDistrict').val();
        var mcode = $('.selGaunagar').val();
        var year = $(this).val();
        console.log(year);
        select_year(year, selected_adminid);
    });



    function pyramidchart(data) {
        var categories = [
            '0-4', '5-9', '10-14', '15-19',
            '20-24', '25-29', '30-34', '35-39', '40-44',
            '45-49', '50-54', '55-59', '60-64', '65-69',
            '70-74', '75-79', '80-84', '85-89', '90 + ',
        ];
        Highcharts.chart('population_pyramid', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Population Pyramid By Age and Sex'
            },
            credits: {
                enabled: false
            },

            xAxis: [{
                categories: categories,
                reversed: false,
                labels: {
                    step: 2,
                    fontSize: '8px'
                }
            }, { // mirror axis on right side
                opposite: true,
                reversed: false,
                categories: categories,
                linkedTo: 0,
                labels: {
                    step: 2,
                    fontSize: '8px'
                }
            }],
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        var ret,
                            numericSymbols = ['k', 'M', 'G', 'T', 'P', 'E'],
                            i = 6,
                            data = Math.abs(this.value);

                        if (data >= 1000) {
                            while (i-- && ret === undefined) {
                                multi = Math.pow(1000, i + 1);
                                if (data >= multi && numericSymbols[i] !== null) {
                                    ret = (data / multi) + numericSymbols[i];
                                }
                            }
                        }

                        return (ret ? ret : this.value);
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
            },

            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointWidth: 6
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                        'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
                }

            },

            series: [{
                name: 'Male',
                color: '#f1c40f',
                data: data['male_range']
            }, {
                name: 'Female',
                color: '#088c6f',
                data: data['female_range']
            }]
        });
    }

    function casteDistribution(data) {
        Highcharts.chart('caste_distri', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'परिवारद्वारा जाति'
                        },
                        subtitle: {
                            text: ' <a href="#" target="_blank"></a>'
                        },
                        xAxis: {
                            type: 'category'
                        },
                        yAxis: {
                            title: {
                                text: 'जम्मा प्रतिशत'
                            }

                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.y:.1f}%'
                                }
                            }
                        },

                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                        },

                        "series": [
                            {
                                "name": "Household By Caste",
                                "colorByPoint": true,
                                "data": data
                            }
                        ]

                    });


        // Highcharts.chart('caste_distri', {
        //     chart: {
        //         plotBackgroundColor: null,
        //         plotBorderWidth: null,
        //         plotShadow: false,
        //         type: 'pie'
        //     },
        //     title: {
        //         text: 'Caste',
        //         style: {
        //             fontWeight: 'bold'
        //         }
        //     },
        //     credits: {
        //         enabled: false
        //     },
        //     tooltip: {
        //         pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //     },
        //     plotOptions: {
        //         pie: {
        //             allowPointSelect: true,
        //             cursor: 'pointer',
        //             dataLabels: {
        //                 enabled: true,
        //                 format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        //                 style: {
        //                     color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
        //                 }
        //             }
        //         }
        //     },
        //     series: [{
        //         name: 'Brands',
        //         colorByPoint: true,
        //         data: data,
        //         showInLegend: false
        //     }]
        // });
    }

    function income(data){
            Highcharts.chart('income_source', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'आय स्रोत'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            "series": [
                {
                    "name": "Income Source",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });

        };

    function destruction(data){
        Highcharts.chart('destruction', {

            title: {
                text: 'विनाश'
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                allowPointSelect: true,
                data: data,
                showInLegend: true
            }]
        });
        }

    function famine(data){
         Highcharts.chart('famine', {

            title: {
                text: 'Famine reason'
            },
             credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                allowPointSelect: true,
                data: data,
                showInLegend: true
            }]
        });

    }

    function language(data) {
        Highcharts.chart('language_distri', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'भाषा अनुसार जनसंख्या'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'जम्मा प्रतिशत'
                }
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Language Percentage: <b>{point.y:.1f} %</b>'
            },
            series: [{
                name: 'Population',
                data: data,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    }

    function death_reason(data){
        Highcharts.chart('death', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'मृत्युको कारण'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'percentage',
                colorByPoint: true,
                data: data
            }]
        });
    }

    function household_ownership(data){
        Highcharts.chart('ownership', {
            chart: {
                type: 'column'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'घरको स्वामित्व'
            },
            subtitle: {
                text: ' <a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }

    function marital_status(data){
        Highcharts.chart('marital', {

            title: {
                text: 'वैवाहिक स्थितिद्वारा जनसंख्या'
            },

            credits: {
                enabled: false
            },
            series: [{
                type: 'pie',
                allowPointSelect: true,
                data: data,
                showInLegend: true
            }]
        });

    }

    function equipments(data){
        Highcharts.chart('equipment', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'उपकरण'
            },
            subtitle: {
                text: ' <a href="#"></a>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "name" :"Percentage",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }

    function tax(data){
        Highcharts.chart('tax', {
            chart: {
                type: 'column'
            }, credits: {
                enabled: false
            },
            title: {
                text: 'कर'
            },
            subtitle: {
                text: ' <a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "name": "Percentage",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });

    }

    function roof(data){
        Highcharts.chart('roof', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'छत संरचनाको रूपमा घर वितरण'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        },
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Percentage',
                data: data
            }]
        });
    }

    function wall(data){
        Highcharts.chart('wall', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'पर्खाल संरचनाको रूपमा घर वितरण'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Percentage',
                colorByPoint: true,
                data: data
            }]
        });
    }

    function water(data){
        Highcharts.chart('water', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'खानेपानीको प्रमुख स्रोत'
            },
            subtitle: {
                text: ' <a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            }, credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    // "name": "खानेपानी",
                    "colorByPoint": true,
                    "data": data
                }
            ],

        });
    }

    function light_source(data){
        Highcharts.chart('light', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'बत्तीको स्रोत'
            },
            subtitle: {
                text: '<a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "name": "बत्तीको स्रोत",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }

    function passed_education(data){
        Highcharts.chart('passedlevel', {
            chart: {
                type: 'column'
            }, credits: {
                enabled: false
            },
            title: {
                text: 'शिक्षित'
            },
            subtitle: {
                text: ' <a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "name": "Percentage",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });

    }

    function totalpassed(data){
        chart = new Highcharts.chart({
            chart: {
                renderTo: 'totalpassed',
                type: 'pie'
            },
            title: {
                text: 'शिक्षित'
            },
            yAxis: {
                title: {
                    text: 'Total percent market share'
                }
            },
           tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Population',
                data: [["कम्तीमा 5 कक्षा पास पारिएको",data[0]],["अशिक्षित",data[1]]],
                size: '60%',
                innerSize: '50%',
                showInLegend:true,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    }
    function studying_education(data){
        Highcharts.chart('studyinglevel', {
            chart: {
                type: 'column'
            }, credits: {
                enabled: false
            },
            title: {
                text: 'शिक्षा अध्ययन गर्दै'
            },
            subtitle: {
                text: ' <a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "name": "Percentage",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }
    function total_studying(data){
        chart = new Highcharts.chart({
            chart: {
                renderTo: 'totalstudying',
                type: 'pie'
            },
            title: {
                text: 'शिक्षा अध्ययन गर्दै'
            },
            yAxis: {
                title: {
                    text: 'Total percent market share'
                }
            },
           tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Population',
                data: [["अध्ययन गर्दै",data[0]],["अशिक्षित",data[1]]],
                size: '60%',
                innerSize: '50%',
                showInLegend:true,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    }
    function agriculture(data) {
        Highcharts.chart('agriculture', {

            title: {
                text: 'कृषि उत्पादन'
            },

            subtitle: {
                text: ''
            },
            credits: {
                enabled: false
            },

            xAxis: {
                type: 'category'
            },

            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },


            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            series: [{
                type: 'column',
                colorByPoint: true,
                data: data,
                showInLegend: false
            }]

        });
    }

    function animal(data){
        Highcharts.chart('animal', {

            title: {
                text: 'पशु'
            },

            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun']
            }, credits: {
                enabled: false
            },

            series: [{
                type: 'pie',
                allowPointSelect: true,
                data: data,
                showInLegend: true
            }]
        });
        }

    function society_improvements(data){
        Highcharts.chart('society', {

            title: {
                text: 'समाज सुधार'
            },

            subtitle: {
                text: ''
            },

            xAxis: {
                type : 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                name : 'society improvements',
                type: 'column',
                colorByPoint: true,
                data: data,
                showInLegend: false
            }]

        });
    }

    function family_improvement(data) {
        Highcharts.chart('family', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            }, credits: {
                enabled: false
            },
            title: {
                text: 'परिवार सुधार'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Percentage',
                colorByPoint: true,
                data: data
            }]
        });
    }

    function destruction_type(data) {
        Highcharts.chart('destructiontype', {
            chart: {
                type: 'column'
            },

            title: {
                text: 'विनाश प्रकार'
            },

            subtitle: {
                text: ''
            }, credits: {
                enabled: false
            },

            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                colorByPoint: true,
                data: data,
                showInLegend: false
            }]

        });
    }

    // function total_destruction(data){
    //     chart = new Highcharts.chart({
    //         chart: {
    //             renderTo: 'totaldestruction',
    //             type: 'pie'
    //         },
    //         title: {
    //             text: 'विनाश प्रकार'
    //         },
    //         yAxis: {
    //             title: {
    //                 text: 'Total percent'
    //             }
    //         },
    //        tooltip: {
    //             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    //         },
    //         credits: {
    //             enabled: false
    //         },
    //         plotOptions: {
    //             pie: {
    //                 allowPointSelect: true,
    //                 cursor: 'pointer',
    //                 dataLabels: {
    //                     enabled: true,
    //                     format: '<b>{point.name}</b>: {point.percentage:.1f} %',
    //                     style: {
    //                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
    //                     }
    //                 }
    //             }
    //         },
    //         series: [{
    //             name: 'Population',
    //             data: [["विनाश",data],["विनाश छैन",3610]],
    //             size: '60%',
    //             innerSize: '50%',
    //             showInLegend:true,
    //             dataLabels: {
    //                 enabled: false
    //             }
    //         }]
    //     });
    //
    // }

    function family_type(data) {
        Highcharts.chart('family_type', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'परिवारको प्रकार द्वारा जनसंख्या'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: data
            }]
        });
    }

    function deathbysex(data) {
        Highcharts.chart('death_by_sex', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Death By Sex'
            },
            credits: {
                enabled: false
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: data['reason'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of person'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:1f} person</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Male',
                data: data['male']

            }, {
                name: 'Female',
                data: data['female']

            }, {
                name: 'Other',
                data: data['other']

            }]
        });
    }

    function toilet(data) {

        Highcharts.chart('toilet', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'चर्पी '
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Percentage',
                colorByPoint: true,
                data: data
            }]
        });

    }

    function energy(data) {

        // Create the chart
        Highcharts.chart('energy', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'ईन्धनको प्रमुख स्रोत'
            },
            subtitle: {
                text: ' <a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            }, credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "name": "ईन्धन",
                    "colorByPoint": true,
                    "data": data
                }
            ],

        });
    }

    function school_type(data){

        Highcharts.chart('schooltype', {

            title: {
                text: 'विद्यालयको प्रकार'
            },

            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr']
            }, credits: {
                enabled: false
            },

            series: [{
                type: 'pie',
                allowPointSelect: true,
                data: data,
                showInLegend: true
            }]
        });

    }

    function education_by_sex(data) {
        //passed education level by sex js start
        Highcharts.chart('passedbysex', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'सेक्स द्वारा शिक्षा'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: data['level'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'total number of people'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Male',
                data: data["male"]

            }, {
                name: 'Female',
                data: data["female"]

            }, {
                name: 'Other',
                data: data["other"]

            }]
        });
    }

    function disease(data) {
        // Build the chart
        $("#disease_percentage").html(data[0].toString());
        //light
        Highcharts.chart('disease', {
            chart: {
                type: 'column'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'रोग प्रतिशत'
            },
            subtitle: {
                text: ' <a href="#" target="_blank"></a>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'जम्मा प्रतिशत'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            "series": [
                {
                    "name": "Disease",
                    "colorByPoint": true,
                    "data": data
                }
            ]
        });
    }

    function total_disease(data){
        chart = new Highcharts.chart({
            chart: {
                renderTo: 'totaldisease',
                type: 'pie'
            },
            title: {
                text: 'रोग'
            },
           tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Population',
                data: [["रोगी",data[0]],["निरोगी ",data[1]]],
                size: '60%',
                innerSize: '50%',
                showInLegend:true,
                dataLabels: {
                    enabled: false
                }
            }]
        });

    }

    function mainfunction_working(data) {
        pyramidchart(data['population_pyramid']);
        income(data['fetch_income_source']);
        destruction(data['fetch_natural_disaster_destruction']);
        famine(data['fetch_famine_reason']);
        casteDistribution(data['fetch_caste']);
        language(data['fetch_language_percentage']);
        death_reason(data['fetch_death_reason']);
        household_ownership(data['fetch_ownership_condition']);
        marital_status(data['fetch_marital_status']);
        equipments(data['fetch_equipments']);
        tax(data['fetch_tax']);
        roof(data['fetch_roof']);
        wall(data['fetch_wall']);
        water(data['fetch_water_source']);
        light_source(data['fetch_light_source']);
        passed_education(data['passed_education_level']);
        totalpassed(data['passed_and_notpassed']);
        studying_education(data['studying_education_level']);
        total_studying(data['studying_notstudying']);
        animal(data['fetch_animal']);
        agriculture(data['fetch_agriculture_production']);
        society_improvements(data['fetch_society_improvements']);
        family_improvement(data['fetch_family_improvements']);
        destruction_type(data['fetch_destruction_type']);
        // total_destruction(data['total_destruction']);
        family_type(data['fetch_family_type']);
        toilet(data['fetch_toilet_type']);
        energy(data['fetch_cooking_fuel']);
        school_type(data['fetch_school_type'])
        deathbysex(data['fetch_death_by_sex']);
        education_by_sex(data['fetch_education_by_sex'])
        disease(data['fetch_disease_percentage'])
        total_disease(data['disease_notdisease'])
        //population infographics
        var html = '';
        var populat = {
            1: [1, 'Total Population'],
            2: [2, 'Male'],
            3: [3, 'Female'],
            4: [4, 'Child (0-14)'],
            5: [5, 'Working (15-64)'],
            6: [6, 'Aging (65+)'],
            7: [7, 'No. of Household'],
            8: [8, 'Average Family Size'],
            9: [9, 'Sex Ratio'],
            10: [10, 'Population Density'],
            11: [11, 'Male House Owner'],
            12: [12, 'Female House Owner']
        }
        for (j = 0; j < 12; j++) {
            // var html='';
            html += '<li><span class="data gutter-lt">';
            html += '<i class="data-icon">';
            html += '<img class="svg-dynamic" src="/static/img/infographics/bank.svg"/>'
            html += '</i><span class="data-item">'
            html += '<span class="title7">' + populat[j + 1][1] + '</span>'
            html += '<small class="font-sm">' + data['population'][j] + '</small></span></span></li>'
        }

        $('#InfoGraphicsD').empty();
        $('#InfoGraphicsD').append(html);
        // $('#passed').empty();
        // var total_passed = '';
        // total_passed += '<li> Total population :'+ data['population'][0] +'</li>';
        // total_passed += '<li> Total passed population :' +  data['total_passed'] + ' </li>'
        // $('#passed').append(total_passed);



        //caste infographics
        var html_caste = '';
        var caste_info = {
            1: "Muslim",
            2: "Bhrahman",
            3: "Chhetri",
            4: "Madhesi",
            5: "Dalit",
            6: "Janajati/Newar"
        };
        for (j = 0; j < 3; j++) {
            // var html_caste='';
            html_caste += '<li><span class="data gutter-lt">';
            html_caste += '<i class="data-icon">';
            html_caste += '<img class="svg-dynamic" src="/static/gaushala/img/infographics/bank.svg"/>'
            html_caste += '</i><span class="data-item">'
            html_caste += '<span class="title7">' + caste_info[j + 1] + '</span>'
            html_caste += '<small class="font-sm">' + data[1][j + 1] + '</small></span></span></li>'
        }
        $('#InfoGraphicsC').empty();
        $('#InfoGraphicsC').append(html_caste)

    }

    function fittoextent(ward) {
        if (ward == 99) {
            MunicipalityLayer.setVisible(true);
            $( "#municipality" ).prop( "checked", true );
        } else {
            MunicipalityLayer.setVisible(false);
            $( "#municipality" ).prop( "checked", false );
        }
        WardLayer.getSource().forEachFeature(function (feature) {
            var ward_no = parseInt(feature.get('new_ward_n'));
            if (ward_no == ward) {
                var stylee = new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: '#00000000'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#1a15d3',
                        width: 2
                    }),
                    text: new ol.style.Text({
                        text: feature.get('new_ward_n'),
                        font: '30px Calibri,sans-serif',
                        fill: new ol.style.Fill({
                            color: '#47eae2'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#2c7dff',
                            width: 1
                        })
                    }),
                    zIndex: 1,
                });

                feature.setStyle(stylee);
            }
            else {
                var stylee = new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: '#00000000'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#a7a8ba',
                        width: 1
                    }),
                    text: new ol.style.Text({
                        text: feature.get('new_ward_n'),
                        font: '12px Calibri,sans-serif',
                        fill: new ol.style.Fill({
                            color: '#15d518'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#000000',
                            width: 1
                        })
                    })
                });

                feature.setStyle(stylee);
            }
        });
        map.getView().fit(extent[ward], map.getSize());
    }

    //Input the ward
    var selected_ward = parseInt($('select[name=selectward]').find(":selected").val());

    //ajax function
    $.ajax({
        url: '/home/allinone/',
        data: {
            ward: selected_ward
        },
        success: function (data) {
            mainfunction_working(data);
        }
    });

    $('select[name=selectward]').on('change', function () {

        var selected_ward = parseInt($('select[name=selectward]').find(":selected").val());
        $.ajax({
            url: '/home/allinone/',
            beforeSend: function (xhr) {
                $("body").addClass("loading");
                //     ajaxStop: function () { $("body").removeClass("loading"); }
            },
            data: {
                ward: selected_ward
            },
            success: function (data) {
                $("body").removeClass("loading");
                mainfunction_working(data);
                fittoextent(selected_ward)
            }
        });

    });

    map.on('click', function (evt) {
        var feature = map.forEachFeatureAtPixel(evt.pixel,
            function (feature, gausala_wards) {
                return feature;
            });
        var ward = {};
        if (feature) {
            var wa = feature.getProperties();
            ward = parseInt(wa.new_ward_n);

            $('select[name=selectward]').val(ward);
            $.ajax({
                url: '/home/allinone/',
                beforeSend: function (xhr) {
                    $("body").addClass("loading");
                },
                data: {
                    ward: ward
                },
                success: function (data) {
                    $("body").removeClass("loading");
                    mainfunction_working(data);
                    fittoextent(ward);
                }
            });
        }
    });
})
;